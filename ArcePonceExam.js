/*Arce Ponce
  Este código JS fué creado para la validación de un formulario
  respetando las condiciones que se dió en el ejercicio.
  El formulario html que se valida estrá en gitlab junto a este archivo :)
*/ 
document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("form").addEventListener('submit', validarFormulario); 
  });
  function validarFormulario(e) {
      e.preventDefault();
      var vcodigo=/^\w{5}$/;
      var vmarc=/^\w{3,50}$/;
      var vmodelo=/^\w{3,30}$/;
      var vanio=/^\d{4}$/;
      
      /*^[a-z-A-Z0-9]{5,5}$*/
      var codigo=document.getElementById('codigo').value;
      var mar=document.getElementById('Marc').value;
      var modelo=document.getElementById('Model').value;
      var anio=document.getElementById('anio').value;
      var inicio=document.getElementById('inicio').value;
      var fin=document.getElementById('fin').value;
      if (!vcodigo.test(codigo)) {
        document.getElementById("c-error").innerHTML = "Codigo invalido(Alfanumerico 5caracteres)";
        return;
      }
      if (!vmarc.test(mar)) {
        document.getElementById("m-error").innerHTML = "Marca no valida(Alfanumerico min3max50caracteres)!";
        return;
    }
    if (!vmodelo.test(modelo)) {
      document.getElementById("mo-error").innerHTML = "Modelo invalido(Alfanumerico min3max30caracteres)!";
      return;
    }
    if (!vanio.test(anio)) {
      document.getElementById("anio-error").innerHTML = "Modelo invalido(Alfanumerico min3max30caracteres)!";
      return;
    }
    if (fin<inicio) {
      document.getElementById("fin-error").innerHTML = "La fecha fin debe ser mayor a la fecha inicial!";
      return;
    }


      this.submit();

  }